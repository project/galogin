<?php

namespace Drupal\ga_login\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Form\UserLoginForm;

/**
 * Provides a user login form.
 */
class NewUserLoginForm extends UserLoginForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['tfa'] = [
      '#description' => $this->t('Enter the one time password'),
      '#type' => 'number',
      '#title' => $this->t('Authentication Code'),
      '#required' => TRUE,
    ];
    return $form;

  }

}
